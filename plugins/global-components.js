import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardPeople from '~/components/card-people'
Vue.component('CardPeople', CardPeople)
