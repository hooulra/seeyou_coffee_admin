import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import memberConstants from '~/store/modules/member/constants'
Vue.prototype.$memberConstants = memberConstants

import attendanceConstants from '~/store/modules/attendance/constants'
Vue.prototype.$attendanceConstants = attendanceConstants

import moneyConstants from '~/store/modules/money/constants'
Vue.prototype.$moneyConstants = moneyConstants

import moneyHistoryConstants from '~/store/modules/money-history/constants'
Vue.prototype.$moneyHistoryConstants = moneyHistoryConstants

import boardConstants from '~/store/modules/board/constants'
Vue.prototype.$boardConstants = boardConstants

import sellConstants from '~/store/modules/sell/constants'
Vue.prototype.$sellConstants = sellConstants

import stockConstants from '~/store/modules/stock/constants'
Vue.prototype.$stockConstants = stockConstants

import productConstants from '~/store/modules/product/constants'
Vue.prototype.$productConstants = productConstants

import productOrderConstants from '~/store/modules/product-order/constants'
Vue.prototype.$productOrderConstants = productOrderConstants

import sellProductConstants from '~/store/modules/sell-product/constants'
Vue.prototype.$sellProductConstants = sellProductConstants

import calculationConstants from '~/store/modules/calculation/constants'
Vue.prototype.$calculationConstants = calculationConstants

import incomeConstants from '~/store/modules/income/constants'
Vue.prototype.$incomeConstants = incomeConstants
