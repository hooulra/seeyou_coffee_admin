const BASE_URL = '/v1/board'

export default {
    DO_DATA: `${BASE_URL}/data`, // set
    DO_BOARD_DETAIL: `${BASE_URL}/board`,
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_LIST: `${BASE_URL}/all`,

    // DO_LIST: `${BASE_URL}/all`, //get
    // DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get

    // DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    // DO_UPDATE: `${BASE_URL}/{id}`, //put
    // DO_CREATE: `${BASE_URL}/new`, //post
}

