import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_DATA]: (store, payload) => {
        return axios.post(apiUrls.DO_DATA, payload)
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page))
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_DETAIL + '?' + paramText)
    },
    // [Constants.DO_LIST_PAGING]: (store, payload) => {
    //     let paramArray = []
    //     if (payload.params.searchName.length >= 1) paramArray.push(`searchName=${payload.params.searchName}`)
    //     if (payload.params.searchAge != null) paramArray.push(`searchAge=${payload.params.searchAge}`)
    //     if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
    //     if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
    //     let paramText = paramArray.join('&')
    //
    //     return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    // },
}
