const BASE_URL = '/v1/product-order'

export default {
    DO_PRODUCT_ORDER_DATA: `${BASE_URL}/list`, //get
    DO_CREATE: `${BASE_URL}/data`
}
