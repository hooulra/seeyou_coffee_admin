import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_PRODUCT_ORDER_DATA]: (store) => {
        return axios.get(apiUrls.DO_PRODUCT_ORDER_DATA)
    },

    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },
}
