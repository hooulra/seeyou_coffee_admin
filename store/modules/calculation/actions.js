import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    // [Constants.DO_LIST_PAGING]: (store, payload) => {
    //     let paramArray = []
    //     if (payload.params.searchName.length >= 1) paramArray.push(`searchName=${payload.params.searchName}`)
    //     if (payload.params.searchAge != null) paramArray.push(`searchAge=${payload.params.searchAge}`)
    //     if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
    //     if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
    //     let paramText = paramArray.join('&')
    //
    //     return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    // },
    // [Constants.DO_DETAIL]: (store, payload) => {
    //     return axios.get(apiUrls.DO_DETAIL.replace('{id}', payload.id))
    // },
    // [Constants.DO_CREATE_COMMENT]: (store, payload) => {
    //     return axios.post(apiUrls.DO_CREATE_COMMENT.replace('{id}', payload.id), payload.list)
    // },
    // [Constants.DO_UPDATE]: (store, payload) => {
    //     return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.list)
    // },
    // [Constants.DO_DELETE]: (store, payload) => {
    //     return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    // },
    // [Constants.DO_CREATE]: (store, payload) => {
    //     return axios.post(apiUrls.DO_CREATE, payload)
    // },
}
