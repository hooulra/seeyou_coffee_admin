import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_SELL_PRODUCT_DATA]: (store) => {
        return axios.get(apiUrls.DO_SELL_PRODUCT_DATA)
    },

    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },

    [Constants.DO_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_DETAIL + '?' + paramText)
    },
}
