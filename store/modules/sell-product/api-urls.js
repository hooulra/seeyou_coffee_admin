const BASE_URL = '/v1/sell-product'

export default {
    DO_SELL_PRODUCT_DATA: `${BASE_URL}/list`, //get
    DO_CREATE: `${BASE_URL}/new`,
    DO_DETAIL: `${BASE_URL}/product`
}
