const BASE_URL = '/v1/stock'

export default {
    DO_DATA: `${BASE_URL}/data`, // 재고등록
    DO_STOCK_DEFICIENT: `${BASE_URL}/lack-list`, // 재고 부족 리스트
    DO_LIST: `${BASE_URL}/list`, // 재고 리스트
    DO_LIST_DETAIL: `${BASE_URL}/stock`, // 재고 리스트 상세
    DO_UPDATE: `${BASE_URL}/update/{id}`, // 재고 수정

    // DO_LIST: `${BASE_URL}/all`, //get
    // DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    // DO_DETAIL: `${BASE_URL}/{id}`, //get
    // DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    // DO_UPDATE: `${BASE_URL}/{id}`, //put
    // DO_CREATE: `${BASE_URL}/new`, //post
}

