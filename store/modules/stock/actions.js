import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_DATA]: (store, payload) => {
        return axios.post(apiUrls.DO_DATA, payload)
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page))
    },
    [Constants.DO_LIST_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_LIST_DETAIL + '?' + paramText)
    },
}
