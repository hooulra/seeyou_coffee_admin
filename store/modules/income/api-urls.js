const BASE_URL = '/v1/income'

export default {
    DO_LIST: `${BASE_URL}/all`, //get
    DO_CREATE: `${BASE_URL}/data`, //post
    DO_DETAIL: `${BASE_URL}/income`,
    DO_UPDATE: `${BASE_URL}/{id}`
}
