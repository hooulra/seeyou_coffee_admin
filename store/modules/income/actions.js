import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_DETAIL + '?' + paramText)
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.list)
    },
    [Constants.DO_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_CREATE, payload)
    },
}
