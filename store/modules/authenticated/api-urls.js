const BASE_URL = '/v1/member'

export default {
    DO_LOGIN: `${BASE_URL}/login/web/owner`, //post
    DO_PASSWORD_CHANGE: `${BASE_URL}/password`, //put
    DO_PROFILE: `${BASE_URL}/profile/info`, //get
}
