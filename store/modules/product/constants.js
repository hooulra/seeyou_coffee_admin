export default {
    DO_LIST: 'product/doList',
    DO_CREATE: 'product/doCreate',
    DO_DETAIL: 'product/doDetail',
    DO_UPDATE: 'product/doUpdate'
}
