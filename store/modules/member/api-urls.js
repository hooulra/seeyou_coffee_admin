const BASE_URL = '/v1/member'

export default {
    DO_CREATE: `${BASE_URL}/data`,
    DO_LIST: `${BASE_URL}/member-list/{page}`,
    DO_DETAIL: `${BASE_URL}/member-lists`,
    DO_UPDATE: `${BASE_URL}/{id}`,
    DO_END: `${BASE_URL}/end/{id}`
}
