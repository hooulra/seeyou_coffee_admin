export default {
    DO_CREATE: 'member/doCreate',
    DO_LIST: 'member/doList',
    DO_DETAIL: 'member/doDetail',
    DO_UPDATE: 'member/doUpdate',
    DO_END: 'member/doEnd'
}
