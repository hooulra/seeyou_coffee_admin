const BASE_URL = '/v1/money'

export default {
    DO_LIST_PAGING: `${BASE_URL}/all`, //get
    DO_CREATE: `${BASE_URL}/member-id/{memberId}`, //post
    DO_DETAIL: `${BASE_URL}/alls`,
    DO_UPDATE: `${BASE_URL}/{id}`
}
