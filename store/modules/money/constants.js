export default {
    DO_LIST_PAGING: 'money/doListPaging',
    DO_CREATE: 'money/doCreate',
    DO_DETAIL: 'money/doDetail',
    DO_UPDATE: 'money/doUpdate'
}
