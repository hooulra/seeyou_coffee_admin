const BASE_URL = '/v1/sell'

export default {
    DO_DATA: `${BASE_URL}/file-upload`, // set
    DO_BILLNUMBER_DETAIL: `${BASE_URL}/billNumber-detail`,
    DO_LIST: `${BASE_URL}/list`,
    DO_REFUND: `${BASE_URL}/refund`, //put

    // DO_LIST: `${BASE_URL}/all`, //get
    // DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    // DO_DETAIL: `${BASE_URL}/{id}`, //get
    // DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    // DO_UPDATE: `${BASE_URL}/{id}`, //put
    // DO_CREATE: `${BASE_URL}/new`, //post
}

