const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            menuLabel: [
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '회원등록', link: '/', isShow: false },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '회원수정', link: '/', isShow: false },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '회원리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: false },
            ]
        },
        {
            parentName: '직원관리',
            menuLabel: [
                { id: 'STAFF_ADD', icon: 'el-icon-tickets', currentName: '직원등록', link: '/member/form', isShow: false },
                { id: 'STAFF_EDIT', icon: 'el-icon-tickets', currentName: '직원수정', link: '/member/edit', isShow: false },
                { id: 'STAFF_LIST', icon: 'el-icon-tickets', currentName: '직원리스트', link: '/member/list', isShow: true },
                { id: 'STAFF_DETAIL', icon: 'el-icon-tickets', currentName: '직원상세정보', link: '/member/detail', isShow: false },
            ]
        },
        {
            parentName: '급여관리',
            menuLabel: [
                { id: 'MONEY_ADD', icon: 'el-icon-tickets', currentName: '급여등록', link: '/money/form', isShow: false },
                { id: 'MONEY_EDIT', icon: 'el-icon-tickets', currentName: '급여수정', link: '/money/edit', isShow: false },
                { id: 'MONEY_LIST', icon: 'el-icon-tickets', currentName: '급여리스트', link: '/money/list', isShow: true },
                { id: 'MONEY_DETAIL', icon: 'el-icon-tickets', currentName: '급여상세정보', link: '/money/detail', isShow: false },
            ]
        },
        {
            parentName: '급여지급내역관리',
            menuLabel: [
                { id: 'MONEY_HISTORY_ADD', icon: 'el-icon-tickets', currentName: '급여지급내역등록', link: '/money-history/form', isShow: false },
                { id: 'MONEY_HISTORY_LIST', icon: 'el-icon-tickets', currentName: '급여지급내역리스트', link: '/money-history/list', isShow: true },
                { id: 'MONEY_HISTORY_DETAIL', icon: 'el-icon-tickets', currentName: '급여지급내역상세정보', link: '/money-history/detail', isShow: false },
            ]
        },
        {
            parentName: '근태관리',
            menuLabel: [
                { id: 'ATTENDANCE_LIST', icon: 'el-icon-tickets', currentName: '근태리스트', link: '/attendance/list', isShow: true },
            ]
        },
        {
            parentName: '상품관리',
            menuLabel: [
                { id: 'PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '상품등록', link: '/product/form', isShow: true },
                { id: 'PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '상품수정', link: '/product/edit', isShow: true },
                { id: 'PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '상품리스트', link: '/product/list', isShow: true },
            ]
        },
        {
            parentName: '판매상품관리',
            menuLabel: [
                { id: 'SELL_PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '판매상품등록', link: '/sell-product/form', isShow: true },
                { id: 'SELL_PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '판매상품수정', link: '/sell-product/edit', isShow: true },
                { id: 'SELL_PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '판매상품리스트', link: '/sell-product/list', isShow: true },
            ]
        },
        {
            parentName: '발주관리',
            menuLabel: [
                { id: 'PRODUCT_ORDER_ADD', icon: 'el-icon-tickets', currentName: '발주등록', link: '/product-order/form', isShow: true },
                { id: 'PRODUCT_ORDER_EDIT', icon: 'el-icon-tickets', currentName: '발주수정', link: '/product-order/edit', isShow: true },
                { id: 'PRODUCT_ORDER_LIST', icon: 'el-icon-tickets', currentName: '발주리스트', link: '/product-order/list', isShow: true },
            ]
        },
        {
            parentName: '판매관리',
            menuLabel: [
                { id: 'SELL_ADD', icon: 'el-icon-tickets', currentName: '판매 등록', link: '/sell/add', isShow: true },
                { id: 'SELL_LIST', icon: 'el-icon-tickets', currentName: '판매 리스트', link: '/sell/list', isShow: true },
            ]
        },
        {
            parentName: '재고관리',
            menuLabel: [
                { id: 'STOCK_ADD', icon: 'el-icon-tickets', currentName: '재고 등록', link: '/stock/add', isShow: true },
                { id: 'STOCK_LIST', icon: 'el-icon-tickets', currentName: '재고 리스트', link: '/stock/list', isShow: true },
            ]
        },
        {
            parentName: '게시판관리',
            menuLabel: [
                { id: 'BOARD_ADD', icon: 'el-icon-tickets', currentName: '게시글 등록', link: '/board/add', isShow: false },
                { id: 'BOARD_LIST', icon: 'el-icon-tickets', currentName: '게시글 리스트', link: '/board/list', isShow: true },
            ]
        },
        {
            parentName: '지출관리',
            menuLabel: [
                { id: 'INCOME_ADD', icon: 'el-icon-tickets', currentName: '지출 등록', link: '/income/form', isShow: false },
                { id: 'INCOME_EDIT', icon: 'el-icon-tickets', currentName: '지출 수정', link: '/income/edit', isShow: false },
                { id: 'INCOME_LIST', icon: 'el-icon-tickets', currentName: '지출 리스트', link: '/income/list', isShow: true },
                { id: 'INCOME_DETAIL', icon: 'el-icon-tickets', currentName: '지출 상세정보', link: '/income/detail', isShow: false },
            ]
        },
        {
            parentName: '마이 메뉴',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
